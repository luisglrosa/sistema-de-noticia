@extends('layouts.app')

@section('titulo','contato')

@section('conteudo')
<div class="container">
    <div class="row">
    <div class="col-8 mx-auto ">
<form action="#" method="POST">
    <div class="form-group">
        <h1>Contato</h1>
        <p>Faça sua sugestão, criticas ou tire suas dúvidas sobre algum conteúdo publicado.</p>
      <label for="exampleFormControlInput1">nome</label>
      <input type="nome" class="form-control" id="exampleFormControlInput1" placeholder="Digite o nome">
    </div>
    <div class="form-group">
        <label for="exampleFormControlInput2">Email address</label>
        <input type="email" class="form-control" id="exampleFormControlInput2" placeholder="name@example.com">
      </div>
      <div class="form-group">
        <label for="exampleFormControlInput3">telefone</label>
        <input type="tel" class="form-control" id="exampleFormControlInput3" placeholder="digite o telefone">
      </div>
    
    <div class="form-group">
      <label for="exampleFormControlTextarea1">mensagem</label>
      <textarea class="form-control" id="exampleFormControlTextarea1" placeholder="mensagem" rows="3"></textarea>
      
    </div>
    <button type="submit" class="btn btn-danger mb-2">Enviar</button>
  </form>
</div>
  </div>
</div>
@endsection