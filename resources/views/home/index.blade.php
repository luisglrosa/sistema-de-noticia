@extends('layouts.app')

@section('titulo','home')

@section('conteudo')
<div class="container">
    <div class="row">
        <article>
            <div class="col-md-4">
                <img class="img-fluid" src="http://via.placeholder.com/400x250">
            </div>
            <div class="col-md-8">
                <h2>Noticia Destaque</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit beatae hic, enim reprehenderit
                    impedit
                    vel consectetur repellat laudantium maxime aut tenetur architecto deleniti asperiores officiis
                    omnis,
                    sed quidem possimus quos!</p>

            </div>
        </article>
    </div>
    <div class="row">
        @for($i = 1; $i <= 3; $i++) 
        <div class="col-md-4 mt-5">
            <article class="card">
                <a href="#">
                    <img class="img-fluid" src="http://via.placeholder.com/500x250"></a>
                <div class="card-body">
                    <h2 class="card-title"><a href="#">Titulo Notícia</a></h2>
                    <p class="card-text">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Facilis quaerat
                        perspiciatis voluptatum adipisci quae ex ab qui delectus reiciendis recusandae nisi vitae
                        temporibus, cum optio accusantium eligendi natus? Repudiandae, consectetur!</p>
                </div>
                <div class="card-footer">
                    30/04/2019
                </div>
            </article>   
        </div>
       @endfor
    </div>
</div>

@endsection
